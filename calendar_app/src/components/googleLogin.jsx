import React from 'react';
import GoogleLogin from 'react-google-login';
import {get_calenders} from './calender';

export default class GoogleLoginComponent extends React.Component{
    constructor(props){
        super(props);
        this.state={
            "clientId":"974862529021-09oqhcaq3cb0vthu2q3jgubpp2q2etdf.apps.googleusercontent.com",
            "clientSecret": "VBaVP02vkjdXW0YlBd-Y29Bl",
            "user":{},
        }
    }

    responseGoogle(res){
        const profileObj = res["profileObj"];
        const userProfile = {
            "name": profileObj["name"],
            "email": profileObj["email"],
            "img": profileObj["imageUrl"]
        }
        console.log("res ", res );
        this.setState({"user": userProfile});

        get_calenders(res.accessToken).then((response) => {
            console.log("=====",response)
        })
 
    }

    render(){
        return(
            <div className="">
                <h1>You will login with google</h1>
                <GoogleLogin 
                    className="google"
                    type="button"
                    clientId={this.state.clientId}
                    buttonText="LOGIN WITH GOOGLE"
                    onSuccess={(res)=>this.responseGoogle(res)}
                    onFailure={(res)=>this.responseGoogle(res)}
                    scope="https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/calendar.events"
                />
                {this.state.user["email"] !== "" && 
                <div>
                    <h2>{this.state.user["name"]}</h2>
                    <h2>{this.state.user["email"]}</h2>
                    <img src={this.state.user["img"]} alt="img not found" />                    
                </div>}
            </div>
        )
    }
}

