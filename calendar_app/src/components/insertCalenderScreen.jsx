import React from 'react';
import GoogleLogin from 'react-google-login';
import {create_cal, addCalendarEvents} from './calender';
import moment from "moment";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import MicroSoftCalendar from './microsoftCalender'

export default class InsertEvent extends React.Component{
    constructor(props){
        super(props);
        this.state={
            "showGoogleBTN": false,
            "clientId":"974862529021-09oqhcaq3cb0vthu2q3jgubpp2q2etdf.apps.googleusercontent.com",
            "clientSecret": "VBaVP02vkjdXW0YlBd-Y29Bl",
            "calendar":{},
            "attendees_arr": [{"email": ""},],
            "start":moment().local().toDate(), 
            "end": moment().local().add(2, 'hours').toDate()
        }
    }

    handleChange(e){
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleEmailChange(i, e){
        const newAttendee = this.state.attendees_arr.map((obj, j)=>{
            if (i !== j){
                // console.log("___",obj);
                return obj
            }
            else{
                return {
                    ...obj, "email": e.target.value
                };
            }
        })
        this.setState({attendees_arr: newAttendee})
    }

    handleCreateEvent(){
        const stateObj = this.state
        const finalObj = {
            "summary": stateObj.summary,
            "description": stateObj.description,
            "location": stateObj.location
        } 
        console.log("===", finalObj);
        this.setState({"showGoogleBTN": true, "calendar": finalObj})
    }

    responseGoogle(res){
        const stateObj = this.state
        const profileObj = res["profileObj"];
        const userProfile = {
            "name": profileObj["name"],
            "email": profileObj["email"],
            "img": profileObj["imageUrl"]   
        }
        this.setState({"user": userProfile, "response": res}, ()=>{
            create_cal(res.accessToken, this.state.calendar).then((data)=>{
                // console.log("data >> ", data );
                const startDateTime = stateObj.start
                const endDateTime = stateObj.end

                const options = { 
                "summary": stateObj.summary,
                "description": stateObj.description,
                "location": stateObj.location,
                "start": {
                    "dateTime":startDateTime,
                },
                "end": {
                    "dateTime": endDateTime,
                },
                "attendees": stateObj.attendees_arr ,
                "anyoneCanAddSelf": true,
                "guestsCanInviteOthers": true,
                "guestsCanModify": true,
                "guestsCanSeeOtherGuests": true,
                }
                addCalendarEvents(res.accessToken, data.id, options).then((response)=>{
                    console.log("===>> ", response);
                    this.setState({"calendarRes": response})
                })
            })
        });
    }

    addMoreAttendes(){
        var attendees_arr = this.state.attendees_arr;
        var email = {"email": this.state.email }
        attendees_arr.push(email)
        // console.log("attendees_arr >> ", attendees_arr);
        this.setState({attendees_arr:attendees_arr})
        return attendees_arr
    }
    
    removeAttendes(i){
        this.setState({
            "attendees_arr": this.state.attendees_arr.filter((obj , j)=> i !== j)
        })
    }

    handleChangeStartDate(e){
        this.setState({"start": e})
    }

    handleChangeEndDate(e){
        this.setState({"end": e})
    }

    handleMSCalendar(){
        return ""
    }

    handleDisable(){
        var summary = this.state.summary;
        if(summary === undefined){
            return true 
        }
        return false
    }

    render(){
        return(
            <div>
                <div className="col-md-6 card card-body" style={{marginLeft: "25%", marginTop: "20px"}}>
                    <div className="text-center" style={{fontWeight: "bold"}}>Add Calendar</div>
                    <div className="row">
                        <div className="col-md-12">
                            <label>summary </label>
                            <input type="text" className="form-control" onChange={(e)=> this.handleChange(e)}  name="summary" placeholder="title of calendar" />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <label>description</label>
                            <textarea rows="5" cols="7" placeholder="description about calendar" className="form-control" ></textarea>
                        </div>
                        <div className="col-md-12">
                            <label>location</label>
                            <input type="text" className="form-control" onChange={(e)=> this.handleChange(e)}  name="location" placeholder="location" />
                        </div>
                    </div> 
                    <div className="row">
                        <div className="col-md-12">
                            <label>add attendees</label>
                            {this.state.attendees_arr.map((obj, i)=>{
                                return(
                                    <div className="row" style={{display: "flex", marginBottom: "10px"}} key={i}>
                                        <div className="col-md-10" >
                                            <input 
                                                type="email" 
                                                value={obj["email"]} 
                                                className="form-control"  
                                                onChange={(e)=> this.handleEmailChange(i, e)} 
                                                name="email" 
                                                placeholder="attendees email" 
                                            />
                                        </div>
                                        <div className="col-md-2">
                                            <button className="btn btn-primary" onClick={()=> this.addMoreAttendes()} style={{marginRight: "10px"}} >+</button>
                                            <button className="btn btn-danger" onClick={()=> this.removeAttendes(i)} >-</button>
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                    <div className="row" style={{display: "flex"}}>
                        {/* <div className="col-md-6">
                            <label>Date</label>
                            <input type="date" name="date" placeholder="select a date" className="form-control" onChange={(e)=> this.handleChange(e)}  />
                        </div>
                        <div className="col-md-6">
                            <label>Time</label>
                            <input type="time" name="time" placeholder="select time" className="form-control" onChange={(e)=> this.handleChange(e)}  />
                        </div> */}
                        <div className="row">
                            <div className="col-md-12" style={{display: "flex"}}>
                                <div className="col-md-6">
                                    <label>start date</label>
                                    <DatePicker
                                        selected={this.state.start}
                                        onChange={(e)=> this.handleChangeStartDate(e)}
                                        showTimeSelect
                                        dateFormat="dd-MM-yyyy h:mm aa"
                                        className="form-control"
                                    />
                                </div>
                                <div className="col-md-6">
                                    <label>end date</label>
                                    <DatePicker
                                        selected={this.state.end}
                                        onChange={(e)=> this.handleChangeEndDate(e)}
                                        showTimeSelect
                                        dateFormat="dd-MM-yyyy h:mm aa"
                                        className="form-control"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    
                    <div className="row">
                        <div className="col-md-6">
                            { this.state.showGoogleBTN === false ? <button className="btn btn-success btn-md text-center" onClick={()=> this.handleCreateEvent()}  disabled={this.handleDisable()} >add gmail calendar </button> : 
                                <GoogleLogin 
                                    className="google"
                                    type="button"
                                    clientId={this.state.clientId}
                                    buttonText="LOGIN WITH GOOGLE"
                                    onSuccess={(res)=>this.responseGoogle(res)}
                                    onFailure={(res)=>this.responseGoogle(res)}
                                    scope="https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/calendar.events"
                                />
                            }
                        </div>
                        <div className="col-md-6">
                            <button className="btn btn-success btn-md text-center"  onClick={()=> this.handleMSCalendar()}>add microsoft calendar</button>
                        </div>
                    </div>
                </div> 
                
                { this.state.calendarRes && <div className=" col-md-6 " style={{marginLeft: "25%", marginTop: "20px"}}>
                    <div className="card">
                        <div className="card-body">
                            <span>
                                your calendar invite for <b> {this.state.calendarRes["start"]["dateTime"]}</b> has been <b className="text-success">{this.state.calendarRes["status"]}</b>
                            </span><br />
                            <span>
                                consider this <a href={this.state.calendarRes["htmlLink"]}> link </a>to join this event 
                            </span>
                        </div>
                    </div>
                </div> }
                <MicroSoftCalendar />
            </div>
        )
    }
}

 