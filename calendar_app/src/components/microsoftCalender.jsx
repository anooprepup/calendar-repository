import React from 'react';
import MicrosoftLogin from "react-microsoft-login";

export default class MicroSoftCalendar extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            "app_client_id": "0d594f70-ffde-408e-a483-547371f1659c",
            "app_tenant_id": "92c7c6d0-893c-4320-afd9-e2e69a94c344",
        }
    }

    authHandler(err, data) {
        console.log("err", err);
        console.log("data ", data);
        
    }

    render(){
        return(
            <div className="App">
                <MicrosoftLogin 
                    clientId={this.state.app_client_id} 
                    authCallback={(err, data)=> this.authHandler(err, data)} 
                    withUserData={true}   
                    // responseType={['code', 'token']}
                    graphScopes={["wl.calendars_update", "user.read", "wl.events_create" ]}
                    // prompt='select_account'
                />
            </div>
        )
    }
}
