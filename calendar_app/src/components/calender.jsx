import fetch from 'isomorphic-fetch';


const BASE_API_URL = "https://www.googleapis.com/calendar/v3"

export function get_calenders(access_token){
    
    return get_api(access_token, "/users/me/calendarList", {});
    return insert_calender(BASE_API_URL);
}

export function create_cal(access_token, data){
    let apiUrl = `${BASE_API_URL}/calendars?access_token=${access_token}`;
    return post_api(apiUrl, data)
}

export function addCalendarEvents(access_token, calendarId, data){

    let apiUrl = `${BASE_API_URL}/calendars/${calendarId}/events?access_token=${access_token}`;
    return post_api(apiUrl, data )
}




function insert_calender(url) {
    var calendar = {
        "summary": "birthday party !",
      }
    var end_point = "/calendars"
    var url =  url+end_point;
    console.log("==",url);
    
    return fetch(url, 
        {
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: calendar
        }).then((response) => {
            // console.log(">>>> ",response.json());
            return response.json();
        });
}

function delete_api(access_token, end_point, params) {

    let apiUrl = `${BASE_API_URL}${end_point}?access_token=${access_token}`;

    Object.keys(params).map((key) => {
        apiUrl = apiUrl + `&${key}=${decodeURIComponent(params[key])}`;
    });

    return fetch(apiUrl, 
        {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json'
            },
        }).then((response) => {
        });
}

function get_api(access_token, end_point, params) {

    let apiUrl = `${BASE_API_URL}${end_point}?access_token=${access_token}`;

    Object.keys(params).map((key) => {
        apiUrl = apiUrl + `&${key}=${decodeURIComponent(params[key])}`;
    });

    return fetch(apiUrl, 
        {
            method: "GET",
            headers: {
                'Content-Type': 'application/json'
            },
        }).then((response) => {
            return response.json();
        });
}

function post_api(url, data) {

    return fetch(url, 
        {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((response) => {
            return response.json();
        });
}